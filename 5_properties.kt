class PropertyExample() {
    var counter = 0
    var propertyWithCounter: Int? = null
        set(value) {
            field = value
            counter++
        }
}


class LazyProperty(val initializer: () -> Int) {
    var isInitialized : Boolean = false
    var initializedValue : Int = 0
    
    val lazy: Int
        get() {
            if (!isInitialized) {
                initializedValue =  initializer()
                isInitialized = true
            }
            return initializedValue
        }
}


import kotlin.reflect.KProperty

class LazyProperty(val initializer: () -> Int) {
    val lazyValue: Int by lazy {
        initializer()
    }
}


class EffectiveDate<R> : ReadWriteProperty<R, MyDate> {

    var timeInMillis: Long? = null

    override fun getValue(thisRef: R, property: KProperty<*>): MyDate {
        return timeInMillis!!.toDate()
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: MyDate) {
        timeInMillis = value.toMillis()
    }
}
