import kotlinx.coroutines.*

fun main() = runBlocking<Unit> {
    launch {
        delay(1000)
        println("World")
    }
    delay(2000)
    println("Hello,")
}
