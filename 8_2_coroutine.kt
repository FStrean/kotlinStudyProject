import kotlinx.coroutines.*

suspend fun delayAndGetNumber1(): Int {
    delay(1000)
    return 1
}

suspend fun delayAndGetNumber2(): Int {
    delay(3000)
    return 3
}

fun main() = runBlocking<Unit> {
    val startTime = System.currentTimeMillis()

    // Последовательный вызов функций
    val resultSequential = delayAndGetNumber1() + delayAndGetNumber2()
    println("Результат последовательного вызова: $resultSequential")
    val endTimeSequential = System.currentTimeMillis()
    println("Время выполнения последовательного вызова: ${endTimeSequential - startTime} мс")

    // Асинхронный вызов функций
    val deferred1 = async { delayAndGetNumber1() }
    val deferred2 = async { delayAndGetNumber2() }
    val resultAsync = deferred1.await() + deferred2.await()
    println("Результат асинхронного вызова: $resultAsync")
    val endTimeAsync = System.currentTimeMillis()
    println("Время выполнения асинхронного вызова: ${endTimeAsync - endTimeSequential} мс")

    // Вывод текста в консоль
    async {
        repeat(3) {
            delay(1000)
            println("I'm sleeping $it ...")
        }
    }
    println("main: I'm tired of waiting!")
    delay(2000)
    println("main: I'm running finally")
    println("main: Now I can quit.")
}
